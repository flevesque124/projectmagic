﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UI.Editor
{
    [CustomEditor(typeof(UICameraPositioner))]
    public class UICameraPositionerEditor : UnityEditor.Editor
    {
        private UICameraPositioner m_Target => (UICameraPositioner) target;

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Set Position"))
            {
                m_Target.SetCameraPosition();
            }
        }

    }

    
}