﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{

    public class HUD : MonoBehaviour
    {
        public GUISkin m_ResourceSkin, m_OrderSkin;

        private const int m_OrderBarWidth = 150, m_ResourceBarHeight = 40;

        private GameObject m_Player;

        private void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
        }

        private void OnGUI()
        {
            if (m_Player != null)
            {
                DrawBar();
            }
        }


        public void DrawBar()
        {
            GUI.skin = m_OrderSkin;
            GUI.BeginGroup(new Rect(Screen.width - m_OrderBarWidth, m_ResourceBarHeight, m_OrderBarWidth, Screen.height - m_ResourceBarHeight));
            GUI.Box(new Rect(0, 0, m_OrderBarWidth, Screen.height - m_ResourceBarHeight), "");
            GUI.EndGroup();
        }

    }
}