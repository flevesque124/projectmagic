﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace UI
{
    //sealed class cannot be inherited
    public sealed class UICameraPositioner : MonoBehaviour
    {
        private Canvas m_Canvas;

        private Canvas Canvas
        {
            get
            {
                if(m_Canvas == null)
                {
                    m_Canvas = transform.FindFirstComponentInAncestor<Canvas>();
                }

                return m_Canvas;
            }
        }

        private void Awake()
        {
            SetCameraPosition();
        }

        public void SetCameraPosition()
        {

            var canvasHalfHeight = 0.5f * ((RectTransform)Canvas.transform).rect.height;
            var canvasHalfFieldOfView = 0.5f * Canvas.worldCamera.fieldOfView;
            var distance = -canvasHalfHeight / Mathf.Tan(canvasHalfFieldOfView * Mathf.Deg2Rad);
            transform.localPosition = new Vector3(0, 0, distance);
        }
    }
}