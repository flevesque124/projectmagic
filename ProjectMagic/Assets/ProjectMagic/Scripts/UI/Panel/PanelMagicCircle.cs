﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class PanelMagicCircle : MonoBehaviour
{
    private UnityEvent m_OnShowEvent = new UnityEvent();
    private UnityEvent m_OnHideEvent = new UnityEvent();
    private bool m_IsActive = false;

    public bool IsPanelActive
    {
        get { return this.m_IsActive; }
        set { this.m_IsActive = value; }
    }

    public void Show()
    {
        gameObject.SetActive(true);
        m_OnShowEvent.Invoke();
    }

    public void Hide()
    {
        m_OnHideEvent.Invoke();
        gameObject.SetActive(false);
    }

    public void Toggle()
    {
        if (m_IsActive == false)
        {
            Show();
            m_IsActive = true;
        }
        else
        {
            Hide();
            m_IsActive = false;
        }
    }
}
