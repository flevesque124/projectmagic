﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicSpell
{
    public abstract class MagicSpell : ScriptableObject
    {
        protected ISpellBehavior m_SpellBehavior;

        public string m_SpellName = "New Magic";
        public Sprite m_SpellIcon;
        public AudioClip m_SpellSound;
        public float m_SpellCoolDown = 0.1f;
        
        public void ApplyCast()
        {

        }

        public void SetCastBehavior(ISpellBehavior castType)
        {

        }
    }    
}