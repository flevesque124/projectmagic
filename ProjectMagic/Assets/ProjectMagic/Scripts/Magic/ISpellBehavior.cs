﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MagicSpell{

    public interface ISpellBehavior
    {
        void Cast();
    }
}