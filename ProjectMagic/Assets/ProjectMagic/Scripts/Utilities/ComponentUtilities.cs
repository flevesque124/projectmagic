﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Utilities
{

    public static class ComponentUtilities
    {
        //T is a generic type parameter that allow you to specify an arbitrary type T to a method at compile
        //time without specify a concrete type ni the method of class declaration
        /*Recusive method that will search through the ancestor of a component until it finds a component of the spicified type*/
        public static T FindFirstComponentInAncestor<T>(this Transform transform, Action<string> tracing = null) where T : Component
        {
            if(tracing == null)
            {
                tracing = Debug.LogError;
            }

            var name = transform.name;

            while(transform.parent != null)
            {
                var component = transform.parent.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }

                transform = transform.parent;
            }

            tracing($"[{name}] The ancestors of {name} do not contain a {typeof(T).Name} component.");

            return null;
        }
    }
}