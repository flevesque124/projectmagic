﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MagicIncantationManager : MonoBehaviour, IDragHandler
{
    #region Variables
    public Canvas MagicChantUI;
    public Text txtMagicSpellName;

    private LineRenderer m_LineRender;

    private bool m_IsRune1Unlock = true;
    private bool m_IsRune2Unlock = true;
    private bool m_IsRune3Unlock = true;
    private bool m_IsRune4Unlock = true;

    private List<int> m_ListCodeMagic = new List<int>();
    private int m_Iteration = 0;

    private bool m_CanBeDraged = true;

    private MagicCodeManager CodeManager;

    #endregion Variables

    // Start is called before the first frame update
    private void Start()
    {
        m_LineRender = GetComponent<LineRenderer>();
        CodeManager = new MagicCodeManager();
        
    }

    private void Update()
    {
        if (m_ListCodeMagic != null)
        {
            if (m_Iteration == 3 && m_ListCodeMagic.Count == 3)
            {
                CodeManager.MagicCall(m_ListCodeMagic);

                //empty the list
                m_ListCodeMagic.Clear();
                m_Iteration = 0;
                
                MagicChantUI.gameObject.SetActive(false);
                
                ResetPosition();
                //unlock the runes
                UnlockRunes();
                return;
            }
        }
        
    }

    
    //event when we drag the UI object
    public void OnDrag(PointerEventData data)
    {
        
        
        if (m_CanBeDraged)
        {
            Vector3 _newPos = Input.mousePosition;

            _newPos.z = transform.position.z;
            
            MovePoint(_newPos);
        }
    }

    public void MovePoint(Vector3 newPos)
    {
        transform.position = newPos;
    }

    public void ResetPosition()
    {
        transform.position = new Vector3(Screen.width/2,Screen.height/2,0f);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Rune1")
        {
            if (m_IsRune1Unlock)
            {
                Debug.Log("Rune1");
                m_IsRune1Unlock = false;
                m_ListCodeMagic.Add(1);
                m_Iteration++;
            }
        }
        else if (collision.tag == "Rune2")
        {
            if (m_IsRune2Unlock)
            {
                m_IsRune2Unlock = false;
                Debug.Log("Rune2");
                m_ListCodeMagic.Add(2);
                m_Iteration++;
            }
        }
        else if (collision.tag == "Rune3")
        {
            if (m_IsRune3Unlock)
            {
                m_IsRune3Unlock = false;
                Debug.Log("Rune3");
                m_ListCodeMagic.Add(3);
                m_Iteration++;
            }
        }
        else if (collision.tag == "Rune4")
        {
            if (m_IsRune4Unlock)
            {
                m_IsRune4Unlock = false;
                Debug.Log("Rune4");
                m_ListCodeMagic.Add(4);
                m_Iteration++;
            }
        }
    }

    public void UnlockRunes()
    {
        m_IsRune1Unlock = true;
        m_IsRune2Unlock = true;
        m_IsRune3Unlock = true;
        m_IsRune4Unlock = true;
    }
}