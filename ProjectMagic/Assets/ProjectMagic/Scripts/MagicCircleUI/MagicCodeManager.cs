﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 This class call the magic casting here
     */

public class MagicCodeManager
{
    public List<int> m_ListCodeMagic;

    public MagicCodeManager()
    {
        
    }

    #region MagicCodeList

    //fire
    private int[] codeMagicFireball = new int[3] { 1, 2, 3 };

    private int[] codeMagicFireBreath = new int[3] { 1, 2, 4 };

    //water
    private int[] codeMagicIceWall = new int[3] { 1, 3, 2 };

    private int[] codeMagicIcelance = new int[3] { 1, 3, 4 };

    //Sky
    private int[] codeMagicThunder = new int[3] { 4, 2, 1 };

    private int[] codeMagicWindSwipe = new int[3] { 4, 3, 1 };

    //earth
    private int[] codeMagicRockSlide = new int[3] { 3, 2, 1 };

    private int[] codeMagicTremor = new int[3] { 3, 4, 1 };

    //light
    private int[] codeMagicShield = new int[3] { 4, 1, 3 };

    //dark
    private int[] codeMagicBlackhole = new int[3] { 4, 2, 3 };

    #endregion MagicCodeList



    //this method call the spell associate to the runes codes list
    public void MagicCall(List<int> runesCode)
    {
        //compare each rune
        if (runesCode[0] == codeMagicFireball[0] && runesCode[1] == codeMagicFireball[1] && runesCode[2] == codeMagicFireball[2])
        {
            Debug.Log("Fireball enable");
        }
        else if (runesCode[0] == codeMagicFireBreath[0] && runesCode[1] == codeMagicFireBreath[1] && runesCode[2] == codeMagicFireBreath[2])
        {
            Debug.Log("fire breath enable");
        }
        else if (runesCode[0] == codeMagicIcelance[0] && runesCode[1] == codeMagicIcelance[1] && runesCode[2] == codeMagicIcelance[2])
        {
            Debug.Log("ice ball enable");
        }
        else if (runesCode[0] == codeMagicIceWall[0] && runesCode[1] == codeMagicIceWall[1] && runesCode[2] == codeMagicIceWall[2])
        {
            Debug.Log("ice wall enable");
        }
        else if (runesCode[0] == codeMagicRockSlide[0] && runesCode[1] == codeMagicRockSlide[1] && runesCode[2] == codeMagicRockSlide[2])
        {
            Debug.Log("rock slide enable");
        }
        else if (runesCode[0] == codeMagicTremor[0] && runesCode[1] == codeMagicTremor[1] && runesCode[2] == codeMagicTremor[2])
        {
            Debug.Log("tremor enable");
        }
        else if (runesCode[0] == codeMagicThunder[0] && runesCode[1] == codeMagicThunder[1] && runesCode[2] == codeMagicThunder[2])
        {
            Debug.Log("thunder enable");
        }
        else if (runesCode[0] == codeMagicWindSwipe[0] && runesCode[1] == codeMagicWindSwipe[1] && runesCode[2] == codeMagicWindSwipe[2])
        {
            Debug.Log("wind swipe enable");
        }
        else if (runesCode[0] == codeMagicShield[0] && runesCode[1] == codeMagicShield[1] && runesCode[2] == codeMagicShield[2])
        {
            Debug.Log("shield enable");
        }
        else if (runesCode[0] == codeMagicBlackhole[0] && runesCode[1] == codeMagicBlackhole[1] && runesCode[2] == codeMagicBlackhole[2])
        {
            Debug.Log("black hole enable");
            
        }
        else
        {
            Debug.Log("no magic");
        }
    }
}
