﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MAGICIANSTATE
{
    None,
    Move,    
    Incantation,
    CastingSpell,
    WaitCasting,
    EndCasting
}

[System.Serializable]
public class MotionController : MonoBehaviour
{
    #region Variables

    //Variables
    [Tooltip("Magician State")]
    public MAGICIANSTATE MAGICIANSTATE;

    //public bool isAttacking = false;
    //public PanelMagicCircle PanelMagicCircle;
    //private Vector3 currentClickTarget = Vector3.zero;
    private Vector3 clickPoint, currentDestination;
    private Vector3 plToHitPoint;

    private MainCameraRayCaster camRayCaster;

    private CharController controller;
    
    //private MagicInstantiateParticle magicInstantiate;
    Animator anim;
    [SerializeField] private float walkMoveStopRadius = 0.1f;
    [SerializeField] private float attackMoveStopRadius = 5f;
    private RaycastHit hitPos;
    private Ray ray;

    private bool canRayCast = true;
    public bool CanRaycast
    {
        get { return this.canRayCast; }
        set { this.canRayCast = value; }
    }

    #endregion


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        
        camRayCaster = Camera.main.GetComponent<MainCameraRayCaster>();
        controller = GetComponent<CharController>();
        currentDestination = transform.position;        
    }

   
    private void FixedUpdate()
    {
        if (canRayCast)
        {

            if (Input.GetMouseButton(0))
            {
                switch (camRayCaster.LayerHit)
                {
                    case Layer.Walkable:
                        currentDestination = camRayCaster.Hit.point;
                        break;                    
                }
            }
                       

            //var playerToClickPoint = currentDestination - transform.position;
            plToHitPoint = currentDestination - transform.position;

            Debug.DrawRay(transform.position, plToHitPoint, Color.magenta);
            
            //make the player stop if it approach the radius
            if (plToHitPoint.magnitude >= walkMoveStopRadius)
            {
                controller.Movement(plToHitPoint);

            }
            else
            {
                controller.Movement(Vector3.zero);
            }
        }
        else
        {
            controller.Movement(Vector3.zero);
        }


        if (Input.GetKeyDown(KeyCode.Z))
        {
            canRayCast = false;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            canRayCast = true;
        }
    }

    public Vector3 ShortDestination(Vector3 destination, float shortening)
    {
        Vector3 reductionVector = (transform.position - destination).normalized * shortening;
        return reductionVector - destination;
    }

    private void OnDrawGizmos()
    {

        // Draw movement gizmos
        Gizmos.color = Color.blue;
        //Gizmos.DrawLine(transform.position, clickPoint);
               
        Gizmos.DrawSphere(currentDestination, 0.15f);
        //Gizmos.DrawSphere(clickPoint, 0.1f);

        // Draw attack sphere
        Gizmos.color = new Color(255f, 0f, 0, .5f);
        Gizmos.DrawWireSphere(transform.position, attackMoveStopRadius);
    }
}
