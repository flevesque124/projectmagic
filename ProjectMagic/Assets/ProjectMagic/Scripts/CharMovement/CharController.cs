﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{
    #region Variables

    [SerializeField] private float groundCheckDistance = 0.1f;
    [SerializeField] private float stationaryTurnSpeed = 180;
    [SerializeField] private float movingTurnSpeed = 360;
    [SerializeField] private float animSpeedMultiplier = 1f;
    //[SerializeField] private float moveSpeedMultiplier = 1f;
    [SerializeField] private float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
   
    private Animator anim;

    //private CollisionFlags = CollisionFlags.None;

    //private float moveSpeed = 5f;

    private bool canMoveLocomotion = true;

    public bool CANMOVE
    {
        set { this.canMoveLocomotion = value; }
        get { return this.canMoveLocomotion; }
    }

    private bool finished_Movement = true;

    private Vector3 target_Pos = Vector3.zero;
    private Vector3 player_Move = Vector3.zero;

    private float player_ToPointDistance;

    //private float gravity = 9.8f;
    private float height;

    private Vector3 currentClickTarget;

    private Vector3 groundNormal = Vector3.zero;
    private bool isGrounded;
    private float turnAmount;
    private float forwardAmount;

    #endregion


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        currentClickTarget = transform.position;
    }
   
    /*
    private bool IsGrounded()
    {
        return collisionFlags == CollisionFlags.CollidedBelow ? true : false;
    }

    public void CalculateHeight()
    {
        if (IsGrounded())
        {
            height = 0f;
        }
        else
        {
            height -= gravity * Time.deltaTime;
        }
    }*/

    #region Movement

    public bool FinishedMovement
    {
        get
        {
            return finished_Movement;
        }
        set
        {
            finished_Movement = value;
        }
    }

    public Vector3 TargetPosition
    {
        get
        {
            return target_Pos;
        }
        set
        {
            target_Pos = value;
        }
    }

   
    //direction of the movement 
    public void Movement(Vector3 move)
    {

        if (move.magnitude > 1f)
        {
            move.Normalize();
        }


        move = transform.InverseTransformDirection(move);
        CheckGroundStatus();
        move = Vector3.ProjectOnPlane(move, groundNormal);

        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;

        ApplyExtraTurnRotation();
        if (canMoveLocomotion)
        {
            //update the animator
            UpdateAnimator(move);
        }
        else
        {
            //UpdateAnimator(Vector3.zero);
            anim.SetFloat("Forward", 0f, 0.1f, Time.deltaTime);
            anim.SetFloat("Turn", 0f, 0.1f, Time.deltaTime);
        }

    }

    private void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }

    public void CheckGroundStatus()
    {

        RaycastHit hitInfo;

        //draw a line to visualize the ground check ray in the scene
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance), Color.red);

        //a small raycast to the ground to check if the player is on the ground
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
        }
    }

    private void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        anim.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        anim.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);

        // calculate which leg is behind, so as to leave that leg trailing in the jump animation
        // (This code is reliant on the specific run cycle offset in our animations,
        // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
        float runCycle = Mathf.Repeat(anim.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);

        // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        if (isGrounded && move.magnitude > 0)
        {
            anim.speed = animSpeedMultiplier;
        }
        else
        {
            // don't use that while airborne
            anim.speed = 1;
        }
    }

#endregion
}
