﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    This Class is the command pattern     
*/

namespace CommandInput
{

    public abstract class Command
    {
        public abstract void Execute(Animator anim, bool isLoop);

        public abstract void ExecuteMagicCircle(Animator anim, bool isLoop);

        //activate UI canvas
        public virtual void ActivateUI(Canvas canvas, bool isActive) { }
    }

    public class MagicIncantation : Command
    {
        public override void Execute(Animator anim, bool isLoop)
        {
            if (isLoop)
            {
                anim.SetBool("isChanting", true);
            }
            else
            {
                anim.SetBool("isChanting", false);
            }
        }

        public override void ExecuteMagicCircle(Animator anim, bool isLoop)
        {
            if (isLoop)
            {
                anim.SetBool("IsMagicCircleActivated", true);
            }
            else
            {
                anim.SetBool("IsMagicCircleActivated", false);
            }
        }

        public override void ActivateUI(Canvas canvas, bool isActive)
        {
            canvas.gameObject.SetActive(isActive);
        }
    }

    public class DoNothing : Command
    {
        public override void Execute(Animator anim, bool isLoop)
        {

        }

        public override void ExecuteMagicCircle(Animator anim, bool isLoop)
        {

        }
    }
}