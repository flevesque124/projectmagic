﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandInput
{
    public class InputHandler : MonoBehaviour
    {
        #region Variables
        public Canvas MagicCircleUI;
        public ParticleSystem particleMagicCircle;
        public PanelMagicCircle m_PanelMagicCircle;
        Animator m_Anim;
        Command m_KeyZ;
        //List<Command> m_OldCommand = new List<Command>();

        Coroutine m_ReplayCoroutine;

        bool m_IsReplaying = true;
        #endregion

        // Start is called before the first frame update
        void Start()
        {
            m_Anim = GetComponent<Animator>();
            
            m_KeyZ = new MagicIncantation();

            particleMagicCircle.Stop(true);
        }

        // Update is called once per frame
        void Update()
        {
            if (m_IsReplaying)
            {
                HandleInput();
            }
        }

        public void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                //m_KeyZ.Execute(m_Anim, true);
                m_KeyZ.ExecuteMagicCircle(m_Anim, true);
                //m_KeyZ.ActivateUI(MagicCircleUI, true);
                particleMagicCircle.Play(true);
                m_PanelMagicCircle.Show();
            }

            if (Input.GetKeyUp(KeyCode.Z))
            {
                //m_KeyZ.Execute(m_Anim, false);
                m_KeyZ.ExecuteMagicCircle(m_Anim, false);
                //m_KeyZ.ActivateUI(MagicCircleUI, false);
                particleMagicCircle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                m_PanelMagicCircle.Hide();
            }
            
        }
    }
}